# Collections

Simple generic collection class

## Install

Via Composer

``` bash
$ composer require zeageorge/collections
```

## Usage

``` php
<?php

declare(strict_types=1);

use zeageorge\collections\TypedCollection;

require 'vendor/autoload.php';

...see example.php

```

