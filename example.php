<?php

declare(strict_types=1);

use Traversable;
use zeageorge\collections\TypedCollection;

require 'vendor/autoload.php';

class Item {
  /** @var int */
  public $id = null;

  /** @var string */
  public $data = null;

  /**
   * Item constructor.
   *
   * @param int    $id
   * @param string $data
   */
  public function __construct(int $id, string $data) {
    $this->id = $id;
    $this->data = $data;
  }
}

class ItemCollection extends TypedCollection {
  /**
   * ItemCollection constructor.
   */
  public function __construct() {
    parent::__construct(Item::class);
  }

  /**
   * @return Traversable|Item[]
   */
  public function getIterator(): Traversable {
    return parent::getIterator();
  }
}

class StringCollection extends TypedCollection {
  /** @var bool */
  private $nullable = false;

  /**
   * StringCollection constructor.
   *
   * @param bool $nullable
   */
  public function __construct(bool $nullable = false) {
    parent::__construct('string');
  }

  /**
   * @param $item
   *
   * @return bool
   */
  protected function checkType($item): bool {
    return is_string($item) || ($this->nullable && $item === null);
  }

  /**
   * @return Traversable|string[]
   */
  public function getIterator(): Traversable {
    return parent::getIterator();
  }
}

class ExampleService {
  /** @var ItemCollection */
  private $items = null;

  /**
   * SomeService constructor.
   *
   * @param ItemCollection $items
   */
  public function __construct(ItemCollection $items) {
    $this->items = $items;
  }

  /**
   *
   * @return void
   */
  public function list() {
    foreach ($this->items as $item) {
      echo $item->data;
    }
  }
}

/**
 * ItemCollection example
 */
$collection = new ItemCollection;
$collection[] = new Item(1, 'A');
$collection[] = new Item(2, 'B');
$collection[] = new Item(3, 'C');

//$collection[] = 42; // InvalidArgumentException: invalid type

$service = new ExampleService($collection);
$service->list();

/**
 * StringCollection example
 */
$strings = new StringCollection;
$strings[] = 'foo';
$strings[] = 'bar';

//$strings[] = 1337; // InvalidArgumentException: invalid type
