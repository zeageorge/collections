<?php

declare(strict_types=1);

namespace zeageorge\collections;

use
  InvalidArgumentException,
  IteratorAggregate,
  Traversable,
  Countable,
  JsonSerializable,
  ArrayAccess,
  ArrayIterator;
use function
  count,
  is_array;

/**
 * Description of TypedCollection
 *
 * @author George Zeakis <zeageorge@gmail.com>
 * @see https://gist.github.com/rickhub/aa6cb712990041480b11d5624a60b53b
 *
 */
class TypedCollection implements JsonSerializable, IteratorAggregate, Countable, ArrayAccess {
  /** @var string */
  protected $type;

  /** @var mixed[] */
  protected $items = [];

  /**
   * Constructor
   *
   * @param string $type
   */
  public function __construct(string $type) {
    $this->type = $type;
  }

  //region IteratorAggregate

  /**
   *
   * @inheritdoc
   */
  public function getIterator(): Traversable {
    return new ArrayIterator($this->items);
  }

  //endregion

  //region ArrayAccess

  /**
   *
   * @inheritdoc
   */
  public function offsetExists($offset): bool {
    return isset($this->items[$offset]);
  }

  /**
   *
   * @inheritdoc
   */
  public function offsetGet($offset) {
    return isset($this->items[$offset]) ? $this->items[$offset] : null;
  }

  /**
   *
   * @param mixed $offset
   * @param mixed $item
   * @return void
   * @throws InvalidArgumentException
   */
  public function offsetSet($offset, $item) {
    if (!$this->checkType($item)) {
      throw new InvalidArgumentException('invalid type');
    }

    $offset !== null ? $this->items[$offset] = $item : $this->items[] = $item;
  }

  /**
   *
   * @inheritdoc
   */
  public function offsetUnset($offset) {
    unset($this->items[$offset]);
  }

  //endregion

  //region Countable

  /**
   *
   * @inheritdoc
   */
  public function count(): int {
    return count($this->items);
  }

  //endregion

  //region JsonSerializable

  /**
   *
   * @return $this->type[]
   */
  public function jsonSerialize(): mixed {
    return $this->items;
  }

  //endregion

  /**
   *
   * @inheritdoc
   */
  public function clear() {
    return $this->items = [];
  }

  /**
   *
   * @inheritdoc
   */
  public function isEmpty(): bool {
    return (bool) count($this->items);
  }

  /**
   *
   * @return $this->type[]
   */
  public function toArray() : array {
    return $this->items;
  }

  /**
   *
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   *
   * @param mixed $item
   * @return bool
   */
  protected function checkType($item): bool {
    return $item instanceof $this->type;
  }

  /**
   *
   * @param string $type
   * @return bool
   */
  public function isTypeOf(string $type): bool {
    return $this->type === $type;
  }

  /**
   *
   * @param array|TypedCollection $items
   * @param bool $append
   * @return TypedCollection
   * @throws InvalidArgumentException
   */
  public function merge($items, bool $append = true): TypedCollection {
    if (!(is_array($items) || $items instanceof TypedCollection)) {
      throw new InvalidArgumentException('invalid $items type');
    }

    foreach ($items as $key => $value) {
      if ($append) {
        $this[] = $value;
      } else {
        $this[$key] = $value;
      }
    }

    return $this;
  }
}
